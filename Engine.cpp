/*
 *   Copyright (C) 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Engine.h"

class Engine::Private {
public:
    Plasma::PopupApplet * parent;
    QObject * model;
    bool popupShown;
};

Engine::Engine(Plasma::PopupApplet * parent)
    : QObject(parent), d(new Private())
{
    d->parent = parent;
    d->model  = 0;
}

bool Engine::popupShown() const
{
    return d->popupShown;
}

void Engine::setPopupShown(bool shown)
{
    if (d->popupShown != shown) {
        d->popupShown = shown;
        emit popupShownChanged();
    }
}

QObject * Engine::model() const
{
    return d->model;
}

void Engine::setModel(QObject * model)
{
    d->model = model;
}

void Engine::reload()
{
    QMetaObject::invokeMethod(d->parent, "reload");
}

