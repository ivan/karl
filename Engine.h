/*
 *   Copyright (C) 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef ENGINE_H_
#define ENGINE_H_

#include <QObject>

#include <Plasma/PopupApplet>

class Engine: public QObject {
    Q_OBJECT

    Q_PROPERTY(bool popupShown READ popupShown WRITE setPopupShown NOTIFY popupShownChanged)
    Q_PROPERTY(QObject * model READ model NOTIFY modelChanged)

public:
    Engine(Plasma::PopupApplet * parent);

    bool popupShown() const;
    void setPopupShown(bool shown);

    QObject * model() const;
    void setModel(QObject * model);

public Q_SLOTS:
    void reload();

Q_SIGNALS:
    void popupShownChanged();
    void modelChanged();

private:
    class Private;
    Private * const d;
};

#endif // ENGINE_H_

