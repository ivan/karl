/*
 *  Copyright (C) 2011  Ivan Cukic <ivan.cukic at kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.qtextracomponents 0.1

Item {
    id: root
    width: 200
    height: 250

    property int minimumWidth: 200
    property int minimumHeight: 250

    Column {
        anchors.fill: parent
        spacing: 10

        PlasmaComponents.TextField {
            id: textSearch
            width: parent.width
        }

        DuckDuckGoInfo {
            width: parent.width
            id: item1
            infoQuery: textSearch.text
        }

        DuckDuckGoInfo {
            width: parent.width
            id: item2
            infoQuery: "okular"
        }
    }
}

