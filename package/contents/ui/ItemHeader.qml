/*
 *  Copyright (C) 2011  Ivan Cukic <ivan.cukic at kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.qtextracomponents 0.1

Column {
    id: itemHeader

    property alias title:       textTitle.text
    property alias description: textDescription.text

    anchors.margins: 8

    Text {
        id: textTitle
        width: parent.width

        text:  "Title"
        elide: Text.ElideRight

        font {
            pointSize: 12
            bold: true
        }
    }

    Text {
        id: textDescription
        width: parent.width
        visible: (text != "")

        text: "Description"
        elide: Text.ElideRight

        font {
            pointSize: 10
        }
    }
}
