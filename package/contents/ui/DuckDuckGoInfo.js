.pragma library

var loadResults = function (infoQuery, resultingModel, header, postInit) {
    if (infoQuery == "") return

    var request = new XMLHttpRequest()
    request.open("GET", "http://api.duckduckgo.com/?q=" + infoQuery + "&format=json&pretty=1", true)

    request.onreadystatechange = function() {
        if (request.readyState == request.DONE && request.status == 200) {
            var response = JSON.parse(request.responseText)

            if (response.Results.size == 0) return

            header.title = response.Heading
            header.description = (response.Definition == "")?
                        response.Abstract : response.Definition;

            resultingModel.clear()

            var resultCount = 0

            var addItem = function (item) {
                if (item == undefined || item.Text == undefined) return;
                resultingModel.append({ "Caption": item.Text, "URL":  item.FirstURL })
                resultCount++
            }

            for (result in response.Results) {
                addItem(response.Results[result])
            }

            for (result in response.RelatedTopics) {
                addItem(response.RelatedTopics[result])
            }

            postInit(resultCount)
        }
    }

    request.send()
}

