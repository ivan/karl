/*   vim:set foldenable foldmethod=marker:
 *
 *   Copyright (C) 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1
import org.kde.qtextracomponents 0.1
import org.kde.plasma.core 0.1 as PlasmaCore

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property alias text: label.text
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    /* }}} */

    /* child objects ----------------------------------{{{ */
    Item {
        id: background
        anchors.fill: parent

        Rectangle {
            opacity: icon.opacity - 0.5
            color:   "#444"
            radius:  4
            anchors.fill: parent
        }

        QIconItem {
            id: icon

            width: 22
            height: 22

            icon: "go-next"
            opacity: 0.5
            Behavior on opacity { NumberAnimation { duration: 300 } }

            anchors {
                right:          parent.right
                verticalCenter: parent.verticalCenter

                leftMargin:   4
                rightMargin:  4
            }
        }

        Text {
            id: label

            text: "Title:"
            verticalAlignment: Text.AlignVCenter
            elide:    Text.ElideRight

            anchors {
                top:     parent.top
                bottom:  parent.bottom
                right:   icon.left
                left:    parent.left

                leftMargin:   4
                rightMargin:  4
                topMargin:    4
                bottomMargin: 4
            }
        }

        MouseArea {
            id: mouseArea
            hoverEnabled: true

            onEntered: icon.opacity = 1
            onExited:  icon.opacity = 0.5

            anchors.fill: parent
        }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

