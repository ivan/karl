/*
 *  Copyright (C) 2011  Ivan Cukic <ivan.cukic at kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.qtextracomponents 0.1

import "DuckDuckGoInfo.js" as DDG

Item {
    id: duckInfoRoot
    state: "empty"

    property string infoQuery: ""

    property int itemHeight: 32

    Behavior on height { NumberAnimation { duration: 300 } }

    onInfoQueryChanged: {
        if (infoQuery == "") {
            duckInfoRoot.state = "empty"
        } else {
            duckInfoRoot.state = "searching"
            searchDelayTimer.stop()
            searchDelayTimer.start()
        }
    }

    Timer {
        id: searchDelayTimer

        interval: 500

        running: false
        repeat: false

        onTriggered: {
            var postInit =
                function (resultCount) {
                    if (resultCount > 3) resultCount = 3;
                    if (resultCount > 0) {
                        duckInfoRoot.state = "showing"
                        listLinks.height = duckInfoRoot.itemHeight * resultCount
                    } else {
                        duckInfoRoot.state = "empty"
                    }

                    header.visible = (header.title != "")
                }

            DDG.loadResults(infoQuery, listLinks.model, header, postInit)
        }
    }

    PlasmaComponents.BusyIndicator {
        id: busyPanel
        running: true

        width:  duckInfoRoot.itemHeight
        height: duckInfoRoot.itemHeight

        anchors.centerIn: parent
    }

    Item {
        id: infoPanel

        anchors.fill: parent

        width: 200

        ItemHeader {
            id: header

            anchors {
                left:   parent.left
                right:  parent.right
                top:    parent.top
            }
        }

        Rectangle {
            color: "#8b6"
            radius: 4

            anchors {
                fill: listLinks

                leftMargin:   -4
                topMargin:    -4
                rightMargin:  -4
                bottomMargin: -4
            }
        }

        ListView {
            id: listLinks
            clip: true

            anchors {
                left:  parent.left
                right: parent.right
                top:   header.bottom

                leftMargin:   4
                rightMargin:  4
                bottomMargin: 4
                topMargin:    8
            }

            model: LinkItemModel {}

            delegate: LinkItem {
                height:   duckInfoRoot.itemHeight
                width:    ListView.view.width
                text:     Caption
            }
        }
    }

    states: [
        State {
            name: "empty"
            PropertyChanges { target: infoPanel; visible: false }
            PropertyChanges { target: busyPanel; visible: false }
            PropertyChanges { target: duckInfoRoot; height: 0 }
        },

        State {
            name: "searching"
            PropertyChanges { target: infoPanel; visible: false }
            PropertyChanges { target: busyPanel; visible: true }
            PropertyChanges { target: duckInfoRoot; height: busyPanel.height }
        },

        State {
            name: "showing"
            PropertyChanges { target: infoPanel; visible: true }
            PropertyChanges { target: busyPanel; visible: false }
            PropertyChanges { target: duckInfoRoot; height: listLinks.height + listLinks.y }
        }

    ]
}
