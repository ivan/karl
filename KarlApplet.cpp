/*
 *   Copyright (C) 2009, 2010 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "KarlApplet.h"
#include "Engine.h"

#include <QDeclarativeEngine>
#include <QDeclarativeContext>

#include <Plasma/DeclarativeWidget>
#include <KDesktopFile>
#include <KDebug>

#include <config-karl.h>

class KarlApplet::Private {
public:
    Plasma::DeclarativeWidget * root;
    KDesktopFile * desktop;
    Engine * engine;

    bool initialized : 1;
};

KarlApplet::KarlApplet(QObject * parent, const QVariantList &args)
  : Plasma::PopupApplet(parent, args), d(new Private())
{
    kDebug() << "Location ###";

    d->initialized = false;
    d->engine = new Engine(this);

    d->desktop = 0;
    d->root = 0;
}

KarlApplet::~KarlApplet()
{
    delete d->desktop;
    delete d->root;
    delete d;
}

void KarlApplet::init()
{
    setPopupIcon("lancelot");

    if (d->initialized) return;


    d->root = new Plasma::DeclarativeWidget();
    d->desktop = new KDesktopFile(KARL_PACKAGE_DIR + "metadata.desktop");

    setGraphicsWidget(d->root);
    d->root->setInitializationDelayed(true);
    d->root->engine()->rootContext()->setContextProperty("engine", d->engine);


    reload();
    d->initialized = true;
}

void KarlApplet::reload()
{
    if (d->initialized) {
        d->root->rootObject()->deleteLater();
    }

    d->root->setQmlPath(KARL_PACKAGE_DIR + "/contents/" + d->desktop->desktopGroup().readEntry("X-Plasma-MainScript"));
}

void KarlApplet::popupEvent(bool show)
{
    d->engine->setPopupShown(show);
    Plasma::PopupApplet::popupEvent(show);
}

QObject * KarlApplet::model() const
{
    return 0;
}

K_EXPORT_PLASMA_APPLET(karlapplet, KarlApplet)

